public class SimpleCalculator {

    public static void main(String[] args) {
        int result = 0;
        for(String s : args){
            result += Integer.parseInt(s);
        }
        System.out.println(result);
    }
}
